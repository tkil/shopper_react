import { IShopItem } from "types";

export const mockShopItems: IShopItem[] = [
  { name: "Broccoli", quantity: 1, isPicked: false },
  { name: "Eggs", quantity: 1, isPicked: false },
  { name: "Carrots", quantity: 2, isPicked: false },
  { name: "Bread", quantity: 1, isPicked: false },
  { name: "Milk", quantity: 1, isPicked: false },
  { name: "Cottage Cheese", quantity: 1, isPicked: false },
];
