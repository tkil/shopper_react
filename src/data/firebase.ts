import { auth as fbAuth, initializeApp, firestore } from "firebase";
import { firebaseConfig } from "@root/firebase.config";
import { log } from "woodchuck";

interface IAuthenticateResult {
  status: string;
  userId: string;
}

let db: firebase.firestore.Firestore;

export const authenticate = async (): Promise<IAuthenticateResult> => {
  try {
    const result = await fbAuth().signInWithPopup(new fbAuth.GoogleAuthProvider());
    const token = (result!.credential as any).accessToken;
    const user = result.user;
    return { status: "success", userId: user!.uid };
  } catch (error) {
    log.warn("Unabled to auth to firebase" + error.message, { error });
    return { status: "failure", userId: "" };
  }
};

export const getCachedUid = (): Promise<string> =>
  new Promise((res) => fbAuth().onAuthStateChanged((user) => res(user ? user.uid : "")));

export const signInAndGetUid = async (): Promise<string> => {
  const userId = await getCachedUid();
  if (!userId) {
    return userId;
  }
  return (await authenticate()).userId;
};

export const firebaseInit = async () => {
  initializeApp(firebaseConfig);
  db = firestore();
};

export const saveShoppingList = async (shoppingListItems: any) => {
  try {
    const uid = await getCachedUid();
    await db.collection("users").doc(uid).collection("data").doc("shoppingList").set({
      items: shoppingListItems,
    });
  } catch (error) {
    log.warn("Error saving shopping list", error);
  }
};

export const readShoppingList = async (): Promise<any> => {
  try {
    const uid = await getCachedUid();
    const data = (await db.collection("users").doc(uid).collection("data").doc("shoppingList").get()).data();
    console.log(data && data.items);
  } catch (error) {
    log.warn("Error saving shopping list", error);
  }
};
