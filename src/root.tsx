import React from "react";
import { render } from "react-dom";
import { App } from "@components/app/app";

export const initReact = () => {
  const rootId = "root";
  const rootElement = document.getElementById(rootId);
  if (rootElement) {
    render(<App />, rootElement);
  } else {
    console.error(`Could not find element with ID: ${rootId}`);
  }
};
