// NPM
import React from "react";
import { render } from "@testing-library/react";

// Project
import { ShopScreen } from "../shopScreen";

describe("Component: ShopScreen", () => {
  describe("render()", () => {
    it("should render without error", () => {
      // Arrange
      const { baseElement } = render(<ShopScreen />);
      // Act
      const element = baseElement.firstChild;
      // Assert
      expect(element).not.toBeNull();
    });
  });
});
