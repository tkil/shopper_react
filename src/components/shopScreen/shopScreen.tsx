import React, { useState } from "react";
import { ShopListItem } from "@root/src/components/shopListItem/shopListItem";
import { sortItems } from "@utils/misc";
import { mockShopItems } from "@data/mock";

import "./shopScreen.css";

export const ShopScreen: React.FC = () => {
  const [shopItems, setShopItems] = useState(sortItems(mockShopItems));

  const handlePick = (itemName: string) => {
    const pickedIndex = shopItems.findIndex((i) => i.name === itemName);
    shopItems[pickedIndex].isPicked = !shopItems[pickedIndex].isPicked;
    setShopItems([...shopItems]);
  };

  const handleIncrease = (itemName: string) => {
    const pickedIndex = shopItems.findIndex((i) => i.name === itemName);
    shopItems[pickedIndex].quantity < 9 && shopItems[pickedIndex].quantity++;
    setShopItems([...shopItems]);
  };

  const handleDecrease = (itemName: string) => {
    const pickedIndex = shopItems.findIndex((i) => i.name === itemName);
    shopItems[pickedIndex].quantity > 0 && shopItems[pickedIndex].quantity--;
    setShopItems([...shopItems]);
  };

  return (
    <div className="shop-screen">
      <ul>
        {shopItems.map((shopItem, key) => (
          <ShopListItem
            shopItem={shopItem}
            key={key}
            onPick={handlePick}
            onIncrease={handleIncrease}
            onDecrease={handleDecrease}
          />
        ))}
      </ul>
    </div>
  );
};
