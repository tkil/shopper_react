// NPM
import React from "react";
import { render } from "@testing-library/react";

// Project
import { JotAdd } from "../jotAdd";

describe("Component: JotAdd", () => {
  describe("render()", () => {
    it("should render without error", () => {
      // Arrange
      const { baseElement } = render(<JotAdd />);
      // Act
      const element = baseElement.firstChild;
      // Assert
      expect(element).not.toBeNull();
    });
  });
});
