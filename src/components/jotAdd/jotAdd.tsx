import React, { useState } from "react";
import Autosuggest, { ChangeEvent } from "react-autosuggest";
import "./jotAdd.css";

interface IProps {
  data?: string;
}

const suggestions = ["Apple", "Banana", "Pear", "Plum"];

const getSuggestionValue = (suggestion: string) => suggestion;

const renderSuggestion = (suggestion: string) => <div>{suggestion}</div>;

export const JotAdd: React.FC<IProps> = (props: IProps) => {
  const [value, setValue] = useState("");
  const [quantity, setQuantity] = useState<number | string>("");

  const handleOnChange = (event: React.FormEvent<any>, { newValue }: Autosuggest.ChangeEvent) => setValue(newValue);

  const forceNumberToRange = (val: number, min: number, max: number): number => {
    if (val < min) {
      return min;
    }
    if (val > max) {
      return max;
    }
    return val;
  };

  const handleQuantityChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const incomingVal = Number(event.target.value.split("").pop() || undefined);
    if (!isNaN(incomingVal)) {
      const newVal = forceNumberToRange(incomingVal, 1, 9);
      setQuantity(newVal);
      return;
    }
    setQuantity("");
  };

  return (
    <div className="jot-add">
      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={() => {}}
        onSuggestionsClearRequested={() => {}}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        inputProps={{
          placeholder: "Type something",
          value: value,
          onChange: handleOnChange,
        }}
      />
      <label className="jot-add__quantity-label">
        Qty:
        <input
          name="quantity"
          className="jot-add__quantity"
          type="number"
          value={quantity}
          onChange={handleQuantityChange}
        ></input>
      </label>
      <button className="jot-add__add-button">Add</button>
    </div>
  );
};

JotAdd.defaultProps = {
  data: "",
};
