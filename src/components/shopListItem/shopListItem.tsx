import React from "react";

import { IShopItem } from "types";
import { emptyShopItem, emptyFunctionVoid } from "@utils/constants";
import "./shopListItem.css";

interface IPropsOptional {
  shopItem?: IShopItem;
  onPick?: (itemName: string) => void;
  onIncrease?: (itemName: string) => void;
  onDecrease?: (itemName: string) => void;
  onDelete?: (itemName: string) => void;
}

export const ShopListItem: React.FC<IPropsOptional> = (props) => {
  const { name, quantity, isPicked } = { ...emptyShopItem, ...props.shopItem };
  const onPick = props.onPick;
  const onIncrease = props.onIncrease ? props.onIncrease : emptyFunctionVoid;
  const onDecrease = props.onDecrease ? props.onDecrease : emptyFunctionVoid;
  const onDelete = props.onDelete;

  const handlePick = onPick ? () => onPick(name) : undefined;
  const handleIncrease = () => onIncrease(name);
  const handleDecrease = () => onDecrease(name);
  const handleDelete = onDelete ? () => onDelete(name) : undefined;
  const cssQuantityPicked = handlePick && isPicked ? "shop-list-item__quantity--picked" : "";
  const cssNamePicked = handlePick && isPicked ? "shop-list-item__name--picked" : "";
  const cssNamePointable = handlePick ? "shop-list-item__name--pointable" : "";

  return (
    <li className="shop-list-item common__item-tile">
      <div className="shop-list-item__info">
        <div className={`shop-list-item__quantity ${cssQuantityPicked}`}>{quantity}</div>
        <div
          className={`shop-list-item__name ${cssNamePointable} ${cssNamePicked}`}
          data-testid="shop-list-item-pick"
          onClick={handlePick}
        >
          {name}
        </div>
      </div>

      <div className="shop-list-item__buttons">
        <button className="shop-list-item__button" data-testid="shop-list-item-increase" onClick={handleIncrease}>
          +
        </button>
        <button className="shop-list-item__button" data-testid="shop-list-item-decrease" onClick={handleDecrease}>
          -
        </button>
        {handleDelete && (
          <button
            className="shop-list-item__button-delete common__button--red"
            data-testid="shop-list-item-delete"
            onClick={handleDelete}
          >
            X
          </button>
        )}
      </div>
    </li>
  );
};

ShopListItem.defaultProps = {
  shopItem: emptyShopItem,
  onPick: undefined,
  onIncrease: emptyFunctionVoid,
  onDecrease: emptyFunctionVoid,
  onDelete: undefined,
};
