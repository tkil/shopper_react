// NPM
import React from "react";
import { render, fireEvent } from "@testing-library/react";

// Project
import { ShopListItem } from "../shopListItem";

describe("Component: ShopListItem", () => {
  describe("render()", () => {
    it("should render without error", () => {
      // Arrange
      const { baseElement } = render(<ShopListItem />);
      // Act
      const element = baseElement.firstChild;
      // Assert
      expect(element).not.toBeNull();
    });

    it("should handle increase quantity", () => {
      // Arrange
      const fnSpy = jest.fn();
      const { getByTestId } = render(<ShopListItem onIncrease={fnSpy} />);
      // Act
      const testEl = getByTestId("shop-list-item-increase");
      fireEvent.click(testEl);
      // Assert
      expect(fnSpy).toBeCalled();
    });

    it("should handle decrease quantity", () => {
      // Arrange
      const fnSpy = jest.fn();
      const { getByTestId } = render(<ShopListItem onDecrease={fnSpy} />);
      // Act
      const testEl = getByTestId("shop-list-item-decrease");
      fireEvent.click(testEl);
      // Assert
      expect(fnSpy).toBeCalled();
    });

    it("should handle pick", () => {
      // Arrange
      const fnSpy = jest.fn();
      const { getByTestId } = render(<ShopListItem onPick={fnSpy} />);
      // Act
      const testEl = getByTestId("shop-list-item-pick");
      fireEvent.click(testEl);
      // Assert
      expect(fnSpy).toBeCalled();
    });

    it("should handle delete", () => {
      // Arrange
      const fnSpy = jest.fn();
      const { getByTestId } = render(<ShopListItem onDelete={fnSpy} />);
      // Act
      const testEl = getByTestId("shop-list-item-delete");
      fireEvent.click(testEl);
      // Assert
      expect(fnSpy).toBeCalled();
    });
  });
});
