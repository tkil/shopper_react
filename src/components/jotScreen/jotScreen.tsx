import React from "react";
import { IShopItem } from "types";
import { emptyShopItem } from "@utils/constants";
import { ShopListItem } from "@components/shopListItem/shopListItem";
import { JotAdd } from "@components/jotAdd/jotAdd";

import "./jotScreen.css";

interface IProps {
  shopItem?: IShopItem;
}

const shopItems: IShopItem[] = [
  { name: "Broccoli", quantity: 1, isPicked: false },
  { name: "Eggs", quantity: 1, isPicked: false },
  { name: "Carrots", quantity: 2, isPicked: false },
  { name: "Bread", quantity: 1, isPicked: false },
  { name: "Milk", quantity: 1, isPicked: false },
  { name: "Cottage Cheese", quantity: 1, isPicked: false },
];

export const JotScreen: React.FC<IProps> = (props) => {
  return (
    <div className="jot-screen">
      <JotAdd />
      <ul>
        {shopItems.map((si, key) => (
          <ShopListItem key={key} shopItem={si} />
        ))}
      </ul>
    </div>
  );
};

JotScreen.defaultProps = {
  shopItem: emptyShopItem,
};
