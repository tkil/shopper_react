// NPM
import React from "react";
import { render } from "@testing-library/react";

// Project
import { JotScreen } from "../jotScreen";

describe("Component: JotScreen", () => {
  describe("render()", () => {
    it("should render without error", () => {
      // Arrange
      const { baseElement } = render(<JotScreen />);
      // Act
      const element = baseElement.firstChild;
      // Assert
      expect(element).not.toBeNull();
    });
  });
});
