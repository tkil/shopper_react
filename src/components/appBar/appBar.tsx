import React, { useEffect, useState } from "react";

import { authenticate, firebaseInit, getCachedUid, saveShoppingList, readShoppingList } from "@root/src/data/firebase";

import "./appBar.css";

const handleAuthClick = () => {
  authenticate();
};

const handleGetAuthClick = async () => {
  const userId = await getCachedUid();
  console.log(userId);
};

const handleSave = async () => {
  await saveShoppingList([
    { name: "Broccoli", quantity: 1, picked: false },
    { name: "Eggs", quantity: 1, picked: false },
    { name: "Carrots", quantity: 2, picked: false },
    { name: "Bread", quantity: 1, picked: false },
    { name: "Milk", quantity: 1, picked: false },
    { name: "Cottage Cheese", quantity: 1, picked: false },
  ]);
};

const handleRead = async () => {
  await readShoppingList();
};

const renderDebugButtons = () => (
  <>
    <button onClick={handleAuthClick}>Auth</button>
    <button onClick={handleGetAuthClick}>GetAuth</button>
    <button onClick={handleSave}>Save</button>
    <button onClick={handleRead}>Read</button>
  </>
);

const renderScreenButtons = () => <></>;

interface IProps {
  screen?: string;
  onScreenChange?: (screen: string) => void;
}
export const AppBar = (props: IProps) => {
  const [isDebugScreen, toggleDebugScreen] = useState(false);
  useEffect(() => {
    firebaseInit();
  }, []);

  const handleToggleDebugScreen = () => {
    toggleDebugScreen(!isDebugScreen);
  };

  return (
    <div className="app-bar">
      <div>Shopper</div>
      <div className="app-bar__buttons">
        {renderScreenButtons()}
        <div onClick={handleToggleDebugScreen}>Debug</div>
      </div>
      {isDebugScreen && <div className="app-bar__buttons">{renderDebugButtons()}</div>}
    </div>
  );
};
