// NPM
import React from "react";
// https://testing-library.com/docs/react-testing-library/cheatsheet
import { render } from "@testing-library/react";

// Project
import { AppBar } from "../appBar";

describe("Component: AppBar", () => {
  describe("render()", () => {
    it("should render without error", () => {
      // Arrange
      const { baseElement } = render(<AppBar />);
      // Act
      const element = baseElement.firstChild;
      // Assert
      expect(element).not.toBeNull();
    });
  });
});
