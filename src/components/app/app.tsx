import React, { useState } from "react";
import { AppBar } from "@components/appBar/appBar";
import { ShopScreen } from "@components/shopScreen/shopScreen";
import { JotScreen } from "@root/src/components/jotScreen/jotScreen";
import { JotAdd } from "@root/src/components/jotAdd/jotAdd";
import { appScreen } from "@utils/constants";

import "./app.css";

export function App(this: any, props: any) {
  const [screen, setScreen] = useState(appScreen.jot);

  return (
    <div className="app">
      <AppBar />
      {screen === appScreen.shop && <ShopScreen />}
      {screen === appScreen.jot && <JotScreen />}
    </div>
  );
}
