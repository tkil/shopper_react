// NPM
import React from "react";
import { render } from "@testing-library/react";

// Project
import { App } from "../app";

describe("Component: App", () => {
  describe("render()", () => {
    it("should render without error", () => {
      // Arrange
      const { baseElement } = render(<App />);
      // Act
      const element = baseElement.firstChild;
      // Assert
      expect(element).not.toBeNull();
    });
  });
});
