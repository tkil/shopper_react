import { IShopItem } from "types";

export const emptyShopItem: IShopItem = { name: "", quantity: 0, isPicked: false };
export const emptyFunctionVoid = () => {};

export const appScreen = {
  shop: "shop",
  jot: "jot",
  debug: "debug",
};
