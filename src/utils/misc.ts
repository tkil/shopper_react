import { IShopItem } from "types";

export const sortItems = (items: IShopItem[]) =>
  items.sort((a, b) => {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  });
