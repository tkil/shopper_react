export interface IShopItem {
  name: string;
  isPicked: boolean;
  quantity: number;
}
